<?php

namespace App\Http\Controllers;

use App\Course;
use App\Filters\Course\DifficultyFilter;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index(Request $request)
    {
        $courses= Course::with('subjects')->filter($request, $this->getFilters())->get();
        return $courses;
    }

    protected function getFilters()
    {
         return[

         ] ;
    }
}
