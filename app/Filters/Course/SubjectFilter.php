<?php


namespace App\Filters\Course;


use App\Filters\ItemFilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class SubjectFilter extends ItemFilterAbstract
{

    public function filter(Builder $builder, $value)
    {
        return $builder->whereHas('subjects',function (Builder $builder) use ($value){
             $builder->where('slug', $value);
        });
    }
}
