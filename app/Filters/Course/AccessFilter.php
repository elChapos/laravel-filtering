<?php


namespace App\Filters\Course;


use App\Filters\ItemFilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class AccessFilter extends ItemFilterAbstract
{
    public function mappings()
    {
        return [
            'free' => true,
            'premium' => false,
        ];
    }

    public function filter(Builder $builder, $key)
    {
        $value = $this->resolveFilterValue($key);
        if ($value === null) {
            return $builder;
        }
        return $builder->where('free', $value);
    }


}
