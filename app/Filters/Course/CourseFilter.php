<?php


namespace App\Filters\Course;


use App\Filters\FiltersAbstract;

class CourseFilter extends FiltersAbstract
{
   protected  $filters=[
        'access'=>AccessFilter::class,
        'difficulty'=>DifficultyFilter::class,
        'type'=>TypeFilter::class,
        'subject'=>SubjectFilter::class,
   ];
}
