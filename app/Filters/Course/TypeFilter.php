<?php


namespace App\Filters\Course;


use App\Filters\ItemFilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class TypeFilter extends ItemFilterAbstract
{
    public function mappings()
    {
      return  [
          'project' =>'project',
          'snippet' =>'snippet',
          'theory' =>'theory',
      ];
    }

    public function filter(Builder $builder, $value)
    {
        $key = $this->resolveFilterValue($value);
        if ($key===null){
            return $builder;
        }
        return $builder->where('type',$key);
    }
}
