<?php


namespace App\Filters\Course;


use App\Filters\ItemFilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class DifficultyFilter extends ItemFilterAbstract
{
    public function mappings()
    {
      return  [
          'beginner' =>'beginner',
          'intermediate' =>'intermediate',
          'advanced' =>'advanced',
      ];
    }

    public function filter(Builder $builder, $value)
    {
        $key = $this->resolveFilterValue($value);
        if ($key===null){
            return $builder;
        }
        return $builder->where('difficulty',$key);
    }
}
