<?php


namespace App\Filters;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

abstract class ItemFilterAbstract
{
    abstract public function filter(Builder $builder, $value);

    public function mappings()
    {
         return [];
    }

    public function resolveFilterValue($key)
    {

         return Arr::get($this->mappings(), $key);
    }

}
