## Laravel Filtering

A simple project to demonstrate how to filter data for your response dynamically from request parameters
e.g 

http://localhost:8000/courses?access=premium&difficulty=advanced&type=project

This application uses a builder and custom filter classes to filter courses according to the params provided in the URL

