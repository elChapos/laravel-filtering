<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'name'=>$name =$faker->sentence,
        'slug'=>\Illuminate\Support\Str::slug($name),
        'free'=>rand(0,1),
        'difficulty'=>$faker->randomElement(['beginner','intermediate','advanced']),
        'type'=>$faker->randomElement(['theory','project','snippet']),
    ];
});
